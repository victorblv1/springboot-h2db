package com.viktorsb.springbooth2db.repo;

import com.viktorsb.springbooth2db.model.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
  List<Customer> findByAge(int age);
}